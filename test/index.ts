import {Survey, SurveyAnswer} from '../'

(async ()=>{
	let repsonseModel = new Survey('./test.db');
	await repsonseModel.clearall();

	let answers:SurveyAnswer[] = [
		{name:'testsurvery', answers:[{name:'agerange', value:'41up'}]},
		{name:'testsurvery', answers:[{name:'agerange', value:'3140'}]},
		{name:'testsurvery', answers:[{name:'agerange', value:'2130'}]},
		{name:'testsurvery', answers:[{name:'agerange', value:'41up'}]},
		{name:'testsurvery', answers:[{name:'agerange', value:'2130'}]},
		{name:'testsurvery', answers:[{name:'agerange', value:'20below'}]},
		{name:'testsurvery', answers:[{name:'agerange', value:'20below'}]},
		{name:'testsurvery', answers:[{name:'agerange', value:'20below'}]},
		{name:'testsurvery', answers:[{name:'agerange', value:'2130'}]}
	]

	for(let answer of answers){
		await repsonseModel.add(answer);
	}


	let report = await repsonseModel.report('testsurvery');
	console.log(JSON.stringify(report));
})()
