### Survey system

- Create questionnaire using JSON format
- Save user answers using API
- Get answer count for each question

### Questionnaire example

Create questionnaire
```js
{
	"name":"testsurvery",
	"title":"Test Survey",
	"questions":[{
		"name":"agerange",
		"title":"How old are you?",
		"type":"radio",
		"answers":[{
			"name": "20below",
			"value": "below 20"
		},{
			"name": "2130",
			"value": "21-30"
		},{
			"name": "3140",
			"value": "31-40"
		},{
			"name": "41up",
			"value": "41 above"
		}]
	}]
}

```

Save answer using survey API
```js
let answers:SurveyAnswer[] = [
	{name:'testsurvery', answers:[{name:'agerange', value:'41up'}]},
	{name:'testsurvery', answers:[{name:'agerange', value:'3140'}]},
	{name:'testsurvery', answers:[{name:'agerange', value:'2130'}]},
	{name:'testsurvery', answers:[{name:'agerange', value:'41up'}]},
	{name:'testsurvery', answers:[{name:'agerange', value:'2130'}]},
	{name:'testsurvery', answers:[{name:'agerange', value:'20below'}]},
	{name:'testsurvery', answers:[{name:'agerange', value:'20below'}]},
	{name:'testsurvery', answers:[{name:'agerange', value:'20below'}]},
	{name:'testsurvery', answers:[{name:'agerange', value:'2130'}]}
]

for(let answer of answers){
	await repsonseModel.add(answer);
}
```

Get Answer count
```js
let report = await repsonseModel.report('testsurvery');
```

Result in
```js
[{"name":"agerange","report":[{"answer":"20below","counts":3},{"answer":"2130","counts":3},{"answer":"3140","counts":1},{"answer":"41up","counts":2}]}]
```
