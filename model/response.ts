import * as Sqlite3 from 'sqlite3';

export interface Answer{
	name:string
	answers:{name:string,value:string}[]
}
export class ResponseModel{
	private db:Sqlite3.Database
	private async _sync(){
		await this._serialize();
		await this._run('create table if not exists response (id integer primary key autoincrement , name varchar(128) not null)')
		await this._run('create table if not exists answer (id integer primary key autoincrement , response_id integer not null,  name varchar(128) not null, answer varchar(128))')
	}
	private async _serialize(){
		if (!this.db){
			var db = new Sqlite3.Database(this.dbpath);
			await new Promise((resolve)=>db.serialize(()=>resolve()))
			this.db = db;
		}
	}
	private _run(sql){
		return new Promise((resolve)=>this.db.run(sql,()=>resolve()))
	}
	private _get(sql){
		return new Promise((resolve)=>this.db.get(sql,(err, row)=>resolve(row)))
	}
	private _all(sql){
		return new Promise((resolve)=>this.db.all(sql, (err, rows)=>resolve(rows)))
	}

	constructor(public dbpath){}

	async clearall(){
		await this._sync()
		await this._run('delete from response')
		await this._run('delete from answer')
	}
	async add(response:Answer){
		await this._sync()
		await this._run(`insert into response (name) values ('${response.name}')`)
		let row = await this._get(`select last_insert_rowid() as lastid`)
		let responseId = row['lastid']
		for(let answer of response.answers){
			await this._run(`insert into answer (response_id, name, answer) values (${responseId}, '${answer.name}', '${answer.value}')`)
		}
	}
	async report(responseName:string){
		await this._sync();
		let nameRows = (await this._all(`select distinct a.name from response r, answer a where a.response_id = r.id and  r.name = '${responseName}'`)) as {name:string}[]
		let names:string[] = [];
		for(let row of nameRows){
			names.push(row.name);
		}

		let results = [];
		for(let name of names){
			let result = (await this._all(`select a.answer, count(a.answer) as counts from answer a, response r where r.name = '${responseName}' and r.id = a.response_id and  a.name = '${name}' group by a.answer`)) as any[]
			results.push({
				name:name,
				report:result
			});
		}
		return results
	}
}
