import {ResponseModel, Answer} from './model/response';
export var Survey = ResponseModel;
export interface SurveyAnswer extends Answer{}
